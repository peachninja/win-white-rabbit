<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            array(4, 2, 2),
            array(6, 3, 2)
        );
    }
    
     //SECTION fail !
    /**
     * @dataProvider multiplyFailProvider
     */
    public function testFailMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyFailProvider(){
        return array(
            //if $amount == 7, always fail
            array(14, 7, 2),
            //if one of $amount and $multiplier are decimal
            array(4.5, 3, 1.5),
            //if estimated - amount is lesser than 0.49
            array(0.02, 0.1, 0.2)


        );
    }
}
