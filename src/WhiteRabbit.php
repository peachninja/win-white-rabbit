<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
     private function parseFile ($filePath)
    {
        //TODO implement this!
        $file = file_get_contents($filePath);
        $trimmed = preg_replace('/[^a-zA-Z0-9]/', '',$file);
        $filetosend = count_chars(strtolower($trimmed), 1);
        return $filetosend;

    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
        $occurrences =  max($parsedFile);
        $keys = array_keys($parsedFile, max($parsedFile));
        return chr($keys[0]);
    }
}