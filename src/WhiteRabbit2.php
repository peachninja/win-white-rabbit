<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
       $array_coin_values =  array(
            '100'   => 100,
            '50'   => 50,
            '20'   => 20,
            '10'  => 10,
            '5'  => 5,
            '2'  => 2,
            '1' => 1
        );
        $array_amount_values =  array(
                '1'   => 0,
                '2'   => 0,
                '5'   => 0,
                '10'  => 0,
                '20'  => 0,
                '50'  => 0,
                '100' => 0
                );
        foreach ($array_coin_values as $key => $value)
        {
            while($amount >= $value && $amount != 0)
            {
                $array_amount_values[$key]++;
                $amount = $amount - $value;

            }

        }

        return $array_amount_values;
    }
}